<?php

require_once 'models/usuario.php';

class usuarioController{
    
    public function index(){
        echo "Controlador: usuario, acción: index";
    }
    
    public function registrar(){
        require_once 'views/usuario/registro.php';
    }
    
    public function save(){
        if(isset($_POST)){
            $nombre= isset($_POST['nombre'])? $_POST['nombre'] : false;
            $apellidos=isset($_POST['apellidos'])? $_POST['apellidos'] : false;
            $email=isset($_POST['email'])? $_POST['email'] : false;
            $password=isset($_POST['password'])? $_POST['password'] : false;      
                    
            
            $error=false;
            $usuario=new Usuario();
            if(!is_string($nombre) || !preg_match("/[a-zA-Z ]+/" ,$nombre)){
                $error=true;
                $usuario->setErrores("El formato del nombre no es correcto<br>");
            }
            if(!is_string($apellidos) || !preg_match("/[a-zA-Z ]+/" ,$apellidos)){
                $error=true;
                $usuario->setErrores("El formato de los apellidos no es correcto<br>");
            }
            if(!is_string($email) || !filter_var($email,FILTER_VALIDATE_EMAIL)){
                $error=true;
                $usuario->setErrores("El formato del E-mail no es correcto<br>");
            }
            if(strlen($password)<5){
                $error=true;
                $usuario->setErrores("El Password debe tener al menos 5 caracteres<br>");
            }
            
            //var_dump($error);
            //var_dump($usuario->getErrores());
           //var_dump($usuario->muestraErrores());
           //die();
            
            
            
            
            if($nombre==true && $apellidos==true && $email==true && $password==true && $error==false){
                
                $usuario->setNombre($nombre);
                $usuario->setApellidos($apellidos);
                $usuario->setEmail($email);
                $usuario->setPassword($password);
                $save=$usuario->save();
                if($save){
                    $_SESSION["register"]="complete";
                }else{
                    $_SESSION["register"]="failed";
                }
            }else{
                $_SESSION["register"]="failed";
                
            }
        }else{
                $_SESSION["register"]="failed";
        }
        
  
        header("Location:".base_url."usuario/registrar");
        
    }
    
    public function login(){
        
        if(isset($_POST)){
            $usuario=new Usuario;
            $usuario->setEmail($_POST['email']);
            $usuario->setPassword($_POST['password']);
            
            $identidad=$usuario->login();
            
            
            if($identidad && is_object($identidad)){
                $_SESSION['identity']=$identidad;
                
                if($identidad->rol=="admin"){
                    $_SESSION['admin']=true;
                }
            }else{
                $_SESSION['error_login']="identificación fallida";
            }
               
        }
        header("Location:".base_url);
        
    }
    
    public function logout(){
        
        if(isset($_SESSION['identity'])){
            unset($_SESSION['identity']);
        }
        
        if(isset($_SESSION['admin'])){
            unset($_SESSION['admin']);
        }
        header("Location:".base_url);
        
    }
}

