<?php

class Utils {

    public static function deleteSession($nombresesion) {
        if (isset($_SESSION[$nombresesion])) {
            $_SESSION[$nombresesion] = null;
            unset($_SESSION[$nombresesion]);
        }
        return $nombresesion;
    }

    public static function isAdmin() {
        if (!isset($_SESSION['admin'])) {
            header("Location:" . base_url);
        } else {
            return true;
        }
    }

    public static function isIdentity() {
        if (!isset($_SESSION['identity'])) {
            header("Location:" . base_url);
        } else {
            return true;
        }
    }

    public static function showCategorias() {
        require_once 'models/categoria.php';
        $categoria = new Categoria();
        $categorias = $categoria->getAll();
        return $categorias;
    }

    public static function statsCarrito() {

        $stats = [
            "count" => 0,
            "total" => 0,
        ];

        if (isset($_SESSION['carrito'])) {
            $stats["count"] = count($_SESSION['carrito']);

            foreach ($_SESSION['carrito'] as $indice => $producto) {
                $stats['total'] += $producto['unidades'] * $producto['precio'];
            }
        }

        return $stats;
    }

    public static function showStatus($estado) {

        $valor = 'confirm';
        if ($estado == 'confirm') {
            $valor = 'confirmado';
        } elseif ($estado == 'preparation') {
            $valor = 'En preparación';
        } elseif ($estado == 'ready') {
            $valor = 'preparado';
        } elseif ($estado == 'sended') {
            $valor = 'enviado';
        }
        
        return $valor;
    }

}
