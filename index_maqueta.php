<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Tienda de Ropa con MVC</title>
        <link rel="stylesheet" href="assets/css/index.css"/>
    </head>
    <body>
        <header>
            <img src="assets/imgs/camiseta1.jpg" alt="">
            <div id="titulo-texto">Tienda de Ropa con MVC</div>
        </header>   
        <nav>
            <ul>
                <li><a href="">Inicio</a></li>
                <li><a href="">Categoría 1</a></li>
                <li><a href="">Categoría 2</a></li>
                <li><a href="">Categoría 3</a></li>
                <li><a href="">Categoría 4</a></li>
                <li><a href="">Carrito</a></li>
            </ul>
        </nav>
        <aside>
            <div class="entrar-web">Entrar a la web</div>
            <div class="campos-form">
            <form action="action">
                <p><label for="usuario">Usuario: </label><br>
                   <input type="text" id="usuario">
                </p>
                <p>
                    <label for="password">Contraseña: </label><br>
                   <input type="text" id="password">
                </p>
            <input type="submit" value="Entrar">
            </form>
            </div>
        </aside>
        <div id="principal">
            
            <div class="producto">
                <img src="assets/imgs/camiseta1.jpg" alt="">
                <h2>Camiseta tipo 1</h2>
                <p>20 €</p>
                <a href="#">Comprar</a>
            </div>
            
             <div class="producto">
                <img src="assets/imgs/camiseta1.jpg" alt="">
                <h2>Camiseta tipo 1</h2>
                <p>20 €</p>
                <a href="#">Comprar</a>
            </div>
            
            
        </div>
        <footer>
             Desarrollado por Alberto Ceballos <?= date('Y')?>
        </footer>
    </body>
</html>
