<div class="div_cuerpo">
    <h1>Carrito de la compra</h1>

    <?php if(isset($_SESSION['carrito']) && count($_SESSION['carrito'])>0) {?>
    <table>   
        <thead>
            <tr>
                <th>Imagen</th>
                <th>Nombre</th>
                <th>Precio</th>
                <th>Unidades</th>
                <th>Eliminar Producto</th>
            </tr>
        </thead>
        <tbody> 
            <?php
            foreach ($carrito as $indice => $elemento) {
                $producto = $elemento['producto'];
                ?>

                <tr>
                    <td>
                        <?php if ($producto->imagen != null): ?>
                            <img src="<?= base_url ?>uploads/images/<?= $producto->imagen ?>" alt="" class="img_carrito">
                        <?php else: ?>
                            <img src="<?= base_url ?>assets/imgs/camiseta1.jpg" alt="" class="img_carrito">
                        <?php endif; ?>
                    </td>
                    <td><a href="<?= base_url ?>producto/ver&id=<?= $producto->id ?>"><?= $producto->nombre ?></a></td>
                    <td><?= $producto->precio ?></td>
                    <td>
                        <?= $elemento['unidades'] ?>
                        <div>
                            <a href="<?= base_url?>carrito/down&index=<?=$indice ?>" class="btn_updown">-</a>
                            <a href="<?= base_url?>carrito/up&index=<?=$indice ?>" class="btn_updown">+</a>
                        </div>
                    </td>
                    <td><a href="<?= base_url?>carrito/delete_producto&index=<?= $indice ?>" class="boton_pedido boton_red btn_eliminar">Eliminar</a></td>
                </tr>


            <?php } ?>
        </tbody>
    </table>
    
    <div class="carrito-footer-left"><a href="<?= base_url?>carrito/delete" class="boton_pedido boton_red">Vaciar carrito</a></div>
    <div class="carrito_footer">
        
        <?php $stats = Utils::statsCarrito(); ?>
        <h3>Precio Total: <?= $stats['total']; ?> €</h3>
        <a href="<?= base_url?>pedido/hacer" class="boton_pedido">Hacer pedido</a>
    </div> 
    
    <?php }else{ ?>
    <h3 class="centrado"> El carrito está vacío. Añade productos para verlos aquí </h3>
    <?php }?>
</div>



