<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>Tienda de Ropa con MVC</title>
        <link rel="stylesheet" href="<?=base_url ?>assets/css/index.css"/>
    </head>
    <body>
        <header>
            <img src="<?=base_url ?>assets/imgs/camiseta1.jpg" alt="">
            <div id="titulo-texto">Tienda de ropa con MVC</div>
        </header>   
        <?php
        $categoria=Utils::showCategorias();
        ?>
        <nav>
            <ul>
                <li><a href="<?=base_url?>">Inicio</a></li>
                <?php while($cat=$categoria->fetch_object()){ ?>
                    <li><a href="<?=base_url?>categoria/ver&id=<?=$cat->id?>"><?= $cat->nombre ?></a></li>
                <?php } ?>
            </ul>
        </nav>

