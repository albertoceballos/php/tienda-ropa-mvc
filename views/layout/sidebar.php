<aside>
    <div class="campos-form">
   
        
     
    <div class="entrar-web">Mi carrito</div></a>
    
    <ul class="lista_menu">
        <?php $stats= Utils::statsCarrito() ?>
        <li>Productos: <?= $stats['count'] ?></li>
        <li>Total: <?=$stats['total']  ?> €</li>
        <a href="<?=base_url ?>carrito/index"><li>Ver el carrito</li></a>
    </ul>
     <?php if(!isset($_SESSION['identity'])):  ?>
    <div class="entrar-web">Entrar a la web</div>
        <form action="<?= base_url . "usuario/login" ?>" method="post">
            <p><label for="usuario">Usuario: </label><br>
                <input type="text" name="email" required>
            </p>
            <p>
                <label for="password">Contraseña: </label><br>
                <input type="text" name="password" required>
            </p>
            <input type="submit" value="Entrar">
        </form>
    <?php else: ?>
        <h3><?= $_SESSION['identity']->nombre." ".$_SESSION['identity']->apellidos ?></h3>
    <?php endif; ?>
      
        <ul class="lista_menu">
            <?php if(isset($_SESSION['identity'])): ?>
            <li><a href="<?= base_url ?>pedido/mis_pedidos">Mis pedidos</a></li>
            <li><a href="<?=base_url?>usuario/logout">Cerrar sesión</a></li>
            <?php else: ?>
            <li><a href="<?=base_url?>usuario/registrar">Registrar Usuario</a></li>
            <?php endif; ?>
            
            <?php if(isset($_SESSION['admin'])): ?>
             <li><a href="<?= base_url ?>pedido/gestion">Gestionar pedidos</a></li>
             <li><a href="<?=base_url?>producto/gestion">Gestionar productos</a></li>
             <li><a href="<?=base_url?>categoria/index">Gestionar categorías</a></li>
            <?php endif; ?>
        </ul>
    </div>
</aside>
<div id="principal">

