<div class="div_cuerpo centrado">
    <?php if(isset($categoria)): ?> 
        <h1><?=$categoria->nombre ?></h1>
    <?php else: ?>
        <h1>La categoria no existe</h1>
    <?php endif; ?>
    
    <?php if($productos_cat->num_rows==0): ?>
        <p>No hay productos de esta categoría </p>
    <?php else:?>
        <?php While($pro=$productos_cat->fetch_object()):?>            
            <div class="producto">
                <a href="<?=base_url ?>producto/ver&id=<?=$pro->id?>">
                    <?php if($pro->imagen !=null): ?>
                        <img src="<?= base_url?>uploads/images/<?=$pro->imagen ?>" alt="">
                    <?php else:?>
                        <img src="<?= base_url?>assets/imgs/camiseta1.jpg" alt="">
                    <?php endif; ?>
                    <h2><?=$pro->nombre ?></h2>
                </a>
                <p><?=$pro->precio ?> €</p>
                <a href="<?= base_url ?>carrito/add&id=<?= $pro->id ?>" class="btn_compra">Comprar</a>
            </div>
        <?php endwhile; ?>
     <?php endif; ?>
        
</div>

