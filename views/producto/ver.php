<div class="div_cuerpo centrado">
    
    <?php if (isset($pro)): ?> 
        <h1><?= $pro->nombre ?></h1>
        
        <div class="detalle_imagen">
        <?php if ($pro->imagen != null): ?>
            <img src="<?= base_url ?>uploads/images/<?= $pro->imagen ?>" alt="">
        <?php else: ?>
            <img src="<?= base_url ?>assets/imgs/camiseta1.jpg" alt="">
        <?php endif; ?>
        </div>
        <div class="detalle_datos">
        <h2><?= $pro->nombre ?></h2>
        <p><?= $pro->precio ?> €</p>
        <a href="<?= base_url ?>carrito/add&id=<?= $pro->id ?>" class="btn_compra">Comprar</a>
        </div>
    <?php else: ?>
        <h1>El producto no existe</h1>
    <?php endif; ?>

</div>


