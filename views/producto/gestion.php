
<div class="div_cuerpo">
<h1>Gestión de productos</h1>

<?php
    if(isset($_SESSION['producto']) && $_SESSION['producto']== "complete"){
        echo "<strong class='alert_green'>Nuevo producto creado correctamente</strong>";
    }elseif (isset($_SESSION['producto']) && $_SESSION['producto']!="complete") {
        echo "<strong class='alert_red'>No se ha podido crear el producto correctamente</strong>";
    }
    if(isset($_SESSION['delete']) && $_SESSION['delete']== "complete"){
        echo "<strong class='alert_green'>Producto eliminado correctamente</strong>";
    }elseif (isset($_SESSION['delete']) && $_SESSION['delete']!="complete") {
        echo "<strong class='alert_red'>No se ha podido eliminar el producto correctamente</strong>";
    }
    Utils::deleteSession('producto');
    Utils::deleteSession('delete');
?>
<table>
    <thead>
        <th>Id</th>
        <th>Nombre</th>
        <th>Precio</th>
        <th>Stock</th>
        <th>Gestión</th>
    </thead>
<?php  while ($prod=$productos->fetch_object()){ ?>
    <tr>
        <td><?=$prod->id ?></td>
        <td><?=$prod->nombre ?></td>
        <td><?=$prod->precio ?></td>
        <td><?=$prod->stock ?></td>
        <td>
            <a href="<?=base_url?>producto/editar&id=<?=$prod->id?>" class="boton_gestion">Editar</a>
            <a href="<?=base_url?>producto/eliminar&id=<?=$prod->id ?>" class="boton_gestion boton_red">Eliminar</a>
        </td>
    </tr>
<?php }?>
</table>
<a class="boton_cat" href="<?php base_url?>crear">Crear Producto</a> 
</div>