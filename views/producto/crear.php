
<div class="div_cuerpo">
    
    <?php if(isset($edit) && isset($pro) && is_object($pro)){ ?>
        <h1> Editar el producto <?= $pro->nombre?></h1>
        <?php $url_action=base_url."producto/save&id=".$pro->id ?>
    <?php } else { ?>
        <h1>Crear nuevo producto</h1>
        <?php $url_action=base_url."producto/save" ?>
    <?php } ?>
    
    <form action="<?=$url_action?>" method="post" enctype="multipart/form-data">
        
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" value="<?= isset($pro) && is_object($pro)? $pro->nombre : ""; ?>">
        
        
        <label for="categoria">Categoria</label>
        <select name="categoria">
            <?php
                $categoria= Utils::showCategorias();
                while ($cat=$categoria->fetch_object()){
            ?>     
                <option value="<?= $cat->id ?>" <?= isset($pro) && is_object($pro) && $cat->id==$pro->categoria_id? "selected" : ""; ?>  ><?=$cat->nombre?></option>";
            <?php
                }          
            ?>
        </select>
        
        <label for="descripcion">Descripción</label>
        <textarea name="descripcion"><?= isset($pro) && is_object($pro)? $pro->descripcion : ""; ?></textarea>
        
        <label for="precio">Precio</label>
        <input type="text" name="precio" value="<?= isset($pro) && is_object($pro)? $pro->precio : ""; ?>">
        
        <label for="stock">Stock</label>
        <input type="number" name="stock" value="<?= isset($pro) && is_object($pro)? $pro->stock : ""; ?>">
       
        <label for="oferta">Oferta</label>
        <input type="text" name="oferta" value="<?= isset($pro) && is_object($pro)? $pro->oferta : ""; ?>">
        
        <label for="imagen">Imagen</label>
        <?php
            if(isset($pro) && is_object($pro) && !empty($pro->imagen)){
                echo '<img src="'.base_url.'uploads/images/'.$pro->imagen.'" class="thumbnail" />';
            }
        ?>
        <input type="file" name="imagen">
        
        <br>
        <input type="submit" value="Guardar">
        
    </form>
</div>

