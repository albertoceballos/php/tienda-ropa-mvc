<div class="div_cuerpo">
    <?php if(isset($gestionar)): ?>
    <h1>Gestionar Pedidos</h1>
    <?php  else: ?>
    <h1>Mis pedidos</h1> 
    <?php endif; ?>  
     <table>   
        <thead>
            <tr>
                <th>Id pedido</th>
                <th>Coste</th>
                <th>Fecha</th>
                <th>Estado</th>
               
            </tr>
        </thead>
        <tbody> 
            <?php while($ped=$pedidos->fetch_object()) {?>
                <tr>
                    <td><a href="<?=base_url ?>pedido/detalle&id=<?=$ped->id ?>"><?= $ped->id ?></a></td>
                    <td><?= $ped->coste ?></td>
                    <td><?= $ped->fecha ?></td>
                    <td><?= Utils::showStatus($ped->estado) ?></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
