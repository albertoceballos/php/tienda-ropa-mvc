<div class="div_cuerpo">

    <h1>Detalles del pedido</h1>
    <?php if (isset($pedido)): ?>
        <?php if (isset($_SESSION['admin'])) : ?>
            <form action="<?= base_url?>pedido/estado" method="post">
                <input type="hidden" value="<?= $pedido->id ?>" name="pedido_id">
                <select name="estado" id="">
                    <option value="confirm" <?= $pedido->estado=='confirm'? "selected" :""  ?> >Confirmado</option>
                    <option value="preparation"  <?= $pedido->estado=='preparation'? "selected" :""  ?> >En preparación</option>
                    <option value="ready"  <?= $pedido->estado=='ready'? "selected" :""  ?> >Preparado</option>
                    <option value="sended" <?= $pedido->estado=='sended'? "selected" :""  ?> >Enviado</option>
                </select>
                <input type="submit" value="Cambiar Estado" />
            </form>

        <?php endif; ?> 
        <h3>Datos usuario:</h3>
        Nombre: <em><?= $pedido->nombre ?> <?= $pedido->apellidos ?></em>
        <br>
        E-Mail: <em> <?= $pedido->email ?></em>
        <h3>Datos del pedido:</h3>
        Estado: <?= Utils::showStatus($pedido->estado) ?>
        <br>
        Número de pedido: <?= $pedido->id ?>
        <br>
        Total a pagar: <?= $pedido->coste ?> €
        <h3>Dirección de envío: </h3>
        <?= $pedido->direccion ?>
        <br>Localidad: <?= $pedido->localidad?>
        <br>Provincia: <?= $pedido->provincia ?> 
        
        <p>
        <h3>Productos:</h3>
        </p>
        <table>
            <?php while ($producto = $productos->fetch_object()): ?>

                <tr>
                    <td>
                        <?php if ($producto->imagen != null): ?>
                            <img src="<?= base_url ?>uploads/images/<?= $producto->imagen ?>" alt="" class="img_carrito">
                        <?php else: ?>
                            <img src="<?= base_url ?>assets/imgs/camiseta1.jpg" alt="" class="img_carrito">
                        <?php endif; ?>
                    </td>
                    <td><a href="<?= base_url ?>producto/ver&id=<?= $producto->id ?>"><?= $producto->nombre ?></a></td>
                    <td><?= $producto->precio ?></td>
                    <td><?= $producto->unidades ?></td>
                </tr>

            <?php endwhile; ?>
        </table>   
    <?php endif; ?>

</div>

