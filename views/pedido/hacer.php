<div class="div_cuerpo">

    <?php if (isset($_SESSION['identity'])): ?>
        <h1>Hacer pedido</h1>
        <a href="<?= base_url ?>carrito/index">Ver los productos y precios del pedido</a>
        <form action="<?= base_url?>pedido/add" method="post">
            <label for="provincia">Provincia</label>
            <input type="text" name="provincia" required>
            <label for="localidad">Ciudad</label>
            <input type="text" name="localidad" required>
            <label for="direccion">Direccion</label>
            <input type="text" name="direccion" required>
            <br>
            <input type="submit" value="Confirmar pedido">
        </form>
    <?php else: ?>
        <h1>Necesitas estar identificado</h1>
        <p>Necesitas estar logueado para realizar tu pedido,loguéate e inténtalo otra vez</p>
    <?php endif; ?>
</div>
