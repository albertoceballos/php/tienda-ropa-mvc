<?php

class Usuario{
    private $id;
    private $nombre;
    private $apellidos;
    private $email;
    private $password;
    private $rol;
    private $imagen;
    private $db;
    private $errores=[];
    
    public function __construct(){
        $this->db=Database::conexion();
    }
            
    function getId() {
        return $this->id;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getApellidos() {
        return $this->apellidos;
    }

    function getEmail() {
        return $this->email;
    }

    function getPassword() {
        return password_hash($this->db->real_escape_string($this->password),PASSWORD_BCRYPT,['cost'=>4]);
    }

    function getRol() {
        return $this->rol;
    }

    function getImagen() {
        return $this->imagen;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setNombre($nombre) {
        $this->nombre = $this->db->real_escape_string($nombre);
    }

    function setApellidos($apellidos) {
        $this->apellidos = $this->db->real_escape_string($apellidos);
    }

    function setEmail($email) {
        $this->email = $this->db->real_escape_string($email);
    }

    function setPassword($password) {
        $this->password = $password;
    }

    function setRol($rol) {
        $this->rol = $rol;
    }

    function setImagen($imagen) {
        $this->imagen = $imagen;
    }
    
    public function getErrores() {
        foreach($this->errores as $error){
            echo $error."<br>";
        }
        //return $this->errores;
    }

    function setErrores($error) {
         array_push($this->errores, $error);
    }

    
    public function save(){
        $sql="INSERT INTO usuarios VALUES (NULL,'{$this->getNombre()}','{$this->getApellidos()}','{$this->getEmail()}','{$this->getPassword()}','user',NULL)";
        $save=$this->db->query($sql);
        $resultado=false;
        if($save==true){
            $resultado=true;
        }else{
            $resultado=false;
        }
        return $resultado;
    }
    
    public function login(){
        
        $email=$this->email;
        $password= $this->password;
        $resultado=false;
        
        
        
        //consulta comprueba si existe usuario
        $sql="SELECT * FROM usuarios WHERE email='$email'";
        $login=$this->db->query($sql);
        
        if($login && $login->num_rows==1){
            $usuario=$login->fetch_object();
            $verifica= password_verify($password, $usuario->password);
         
            if($verifica){
                $resultado=$usuario;
            }
             
        }
        return $resultado;
    }
    
  
}


